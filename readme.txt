A Pictionary game implementation in Java. Players can connect to a server and play against other players.
IMPORTANT! For some reason you can't run these programs with JRE 1.7!
I had problems with client freezes/crashes until I switched to JRE 1.6, which was originally used when developing this game.
I'll have to look into this... It seems to work fine with JRE 1.6 though.

Server info:
Reads settings from a file "settings.txt" in the same directory, if the file is not present it will be generated by the server.
In the settings file you can specify
	Hostname (e.g. 127.0.0.1, localhost or 123.45.67.89)
	Port
	Word category
	Path to file containing words
Client info:
When you start the client you have to specify the server's hostname and port.
You will then connect to the server and chose a user name, then you are free to start playing by clicking Join game.
The chat is global and will be seen by everone connected to the server.
Draw by dragging your mouse on the large white area.
When you want to guess on a word you will have to select the field under "Write your guess here" and press enter.