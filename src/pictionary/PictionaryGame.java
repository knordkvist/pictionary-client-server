package pictionary;

import java.util.*;

/**
 * Implementation of a game of Pictionary. <br />
 * This class contains a collection of {@link PictionaryPlayer}, methods for
 * interacting with the players and methods and variables to keep track of and
 * advance the game state.
 * 
 * @author      Kristoffer Nordkvist
 */
public class PictionaryGame
{
    // TODO: Rensa upp.
    /**
     * There needs to be more players before a new game can start.
     */
    public final static int NOT_ENOUGH_PLAYERS_TO_START = 1;

    /**
     * The game has started, and is playing.
     */
    public final static int GAME_STARTED = 2;

    /**
     * The game cannot continue because someone left in the middle of it.
     */
    public final static int NOT_ENOUGH_PLAYERS_TO_CONTINUE = 3;

    /**
     * Someone recently finished drawing, the turn has ended.
     */
    public final static int TURN_END = 4;

    /**
     * All players have drawn, a new round is about to start.
     */
    public final static int ROUND_FINISHED = 5;

    /**
     * The standard pause in seconds. Used when scheduling the next action using
     * a timer.
     */
    public final static int pauseBetweenActions = 5;

    /**
     * The time, in seconds, a player can draw before his time is up.
     */
    public final int drawTimeLimit;

    /**
     * The game's current status. <br />
     * Equals one of the static state descriptions, for example
     * {@link PictionaryGame#GAME_STARTED}
     */
    private int status;

    /**
     * The collection of players currently playing (drawing or guessing).
     */
    private List<PictionaryPlayer> pictionaryPlayers = new ArrayList<PictionaryPlayer>();

    /**
     * The current player; the person who is drawing, is about to draw or was drawing.
     */
    private PictionaryPlayer currentPlayer;

    /**
     * Some text describing the current word category, for example "Animals".
     */
    private String category;

    /**
     * The list of words used in this Pictionary game.
     */
    private ArrayList<String> words;

    /**
     * The list of unused words.
     */
    private ArrayList<String> unusedWords;

    /**
     * The current word being drawn.
     */
    private String currentWord;

    /**
     * The collection holding all the players waiting to join.
     */
    private ArrayList<PictionaryPlayer> newPlayers = new ArrayList<PictionaryPlayer>();

    /**
     * Used for getting a new random word.
     */
    private Random randomGenerator = new Random();

    /**
     * How many turns have been played.
     */
    private int turnsPlayed;

    /**
     * The timer used to create pauses between turns and rounds.
     */
    private Timer timer;

    /**
     * Used for avoiding the canceling of an extra draw round from the timer;
     * sometimes a draw round is canceled prematurely by a correct word guess
     * from a player, then we increment this counter.
     */
    private volatile int skipEndDrawTasks = 0;

    /**
     * Indicates whether a player has made a correct guess.
     */
    private boolean correctGuess;

    /**
     * The constructor.
     * 
     * @param category          Some text describing the word category, for example "Animals".
     * @param words             The words that players will attempt to draw and guess.
     * @param drawTimeLimit     How many seconds a player can draw before the time is up and
     *                          the turn ends.
     */
    public PictionaryGame(String category, ArrayList<String> words,
            int drawTimeLimit)
    {
        timer = new Timer();
        this.category = category;
        this.words = words;
        this.drawTimeLimit = drawTimeLimit;
        this.status = NOT_ENOUGH_PLAYERS_TO_START;
        unusedWords = new ArrayList<String>(words);
    }

    /**
     * A simple class, overriding {@link TimerTask#run()}, used when scheduling
     * the game's next action.
     */
    private class NextActionTask extends TimerTask
    {
        /*
         * Simply calls nextAction().
         */
        @Override
        public void run()
        {
            nextAction();
        }
    }

    /**
     * A simple class, overriding {@link TimerTask#run()}, used when scheduling
     * the game's next action, created when a player starts drawing.
     */
    private class EndDrawTask extends TimerTask
    {
        /**
         * Calls {@link PictionaryGame#nextAction()} if we aren't supposed to
         * skip it; sometimes a player's draw turn is interrupted and
         * {@link PictionaryGame#nextAction()} is called when a guessing player
         * makes a correct guess, but the timer is still ticking and we want to
         * avoid calling {@link PictionaryGame#nextAction()} an extra time.
         */
        @Override
        public void run()
        {
            if (skipEndDrawTasks > 0)
            {
                skipEndDrawTasks -= 1;
                return;
            }
            nextAction();
        }
    }

    /**
     * Gets the current game status.
     * 
     * @return A number equal to one of the static state descriptors in this
     *         class, for example {@link PictionaryGame#GAME_STARTED}.
     */
    public int getStatus()
    {
        return status;
    }

    /**
     * Starts a new game of Pictionary: Resets the players' scores, lets waiting
     * players join in, tells everyone that the game has started, calls
     * {@link PictionaryGame#nextAction()} etc.
     */
    private void startNewGame()
    {
        correctGuess = false;
        pictionaryPlayers.addAll(newPlayers);
        newPlayers.clear();
        if (pictionaryPlayers.size() < 2)
        {
            status = NOT_ENOUGH_PLAYERS_TO_START;
            return;
        }
        for (PictionaryPlayer player : pictionaryPlayers)
        {
            player.setScore(0);
            player.setStatus(PictionaryPlayer.WAITING_FOR_TURN);
        }

        currentPlayer = pictionaryPlayers.get(0);
        turnsPlayed = 0;
        resetPlayers();
        status = GAME_STARTED;
        tellPlayers(GAME_STARTED);
        nextAction();
    }

    /**
     * Alerts the players that an event has occurred.
     */
    private void tellPlayers(int event)
    {
        ArrayList<PictionaryPlayer> allPlayers = new ArrayList<PictionaryPlayer>();
        allPlayers.addAll(pictionaryPlayers);
        allPlayers.addAll(newPlayers);
        if (event == GAME_STARTED)
        {
            for (PictionaryPlayer player : allPlayers)
            {
                player.gameStarted();
            }
        }
    }

    /**
     * Alerts all players, except the excluded one, that an event has occurred.
     */
    private void tellPlayers(int event, PictionaryPlayer exclude)
    {
        ArrayList<PictionaryPlayer> allPlayers = new ArrayList<PictionaryPlayer>();
        allPlayers.addAll(pictionaryPlayers);
        allPlayers.addAll(newPlayers);
        for (PictionaryPlayer player : allPlayers)
        {
            if (player.equals(exclude))
            {
                continue;
            }
            if (event == PictionaryPlayer.DRAWING)
            {
                player.playerStartedDrawing(exclude.getClientID());
                if (!newPlayers.contains(player))
                {
                    player.startGuessing();
                }
            }
            else if (event == PictionaryPlayer.WAITING_FOR_TURN)
            {
                player.playerStoppedDrawing(exclude.getClientID());
                if (!correctGuess)
                {
                    player.tellWord(currentWord);
                }

            }
        }
    }

    /**
     * Gets the current category.
     * 
     * @return A string describing the current category, for example "Animals".
     */
    public String getCategory()
    {
        return category;
    }

    /**
     * Resets all players' scores and sets their status to
     * {@link PictionaryPlayer#WAITING_FOR_TURN}.
     */
    public void resetPlayers()
    {
        for (PictionaryPlayer player : pictionaryPlayers)
        {
            player.setScore(0);
            player.setStatus(PictionaryPlayer.WAITING_FOR_TURN);
        }

    }

    /**
     * Sets the current word to a new random word and removes that word from
     * unusedWords.
     */
    private void nextRandomWord()
    {
        if (unusedWords.size() == 0)
        {
            // Vi har slut p� ord, �teranv�nd gamla ord
            unusedWords = new ArrayList<String>(words);
        }
        int currentWordIndex = randomGenerator.nextInt(unusedWords.size());
        currentWord = unusedWords.remove(currentWordIndex);
        correctGuess = false;
    }

    /**
     * Advances the game to a new state depending on the game's current state. <br />
     * Gets the next random word when a new turn is about to start, tells each
     * player when its their turn to start or stop drawing/guessing and informs
     * them of different events that occurs - for example when a player makes a
     * correct guess - and schedules timers that trigger the next action when
     * the time is up.
     */
    public void nextAction()
    {
        if (pictionaryPlayers.size() < 2)
        {
            status = NOT_ENOUGH_PLAYERS_TO_CONTINUE;
            return;
        }
        if (status == TURN_END)
        {
            status = GAME_STARTED;
        }
        if (status == ROUND_FINISHED)
        {
            startNewGame();
        }
        else if (status == GAME_STARTED || status == TURN_END)
        {
            if (currentPlayer.getStatus() == PictionaryPlayer.WAITING_FOR_TURN)
            {
                nextRandomWord();
                currentPlayer.setStatus(PictionaryPlayer.DRAWING);
                tellPlayers(PictionaryPlayer.DRAWING, currentPlayer);

                currentPlayer.startDraw(currentWord);
                scheduleEndDrawTask();
            }
            else if (currentPlayer.getStatus() == PictionaryPlayer.DRAWING)
            {
                currentPlayer.stopDraw();
                turnsPlayed += 1;
                tellPlayers(PictionaryPlayer.WAITING_FOR_TURN, currentPlayer);
                currentPlayer.setStatus(PictionaryPlayer.WAITING_FOR_TURN);
                // Om inte alla har f�tt rita den h�r omg�ngen
                if (turnsPlayed < pictionaryPlayers.size())
                {
                    turnFinished();
                }
                else
                {
                    roundFinished();

                }
                scheduleNextTask(pauseBetweenActions);
            }
        }
        else if (status == ROUND_FINISHED)
        {
            System.out
                    .println("status == ROUND_FINISHED, everyone has had their turn. Time to start a new game");
            scheduleNextTask(pauseBetweenActions * 2);
        }
    }

    /**
     * Schedules the next game state update, the method will be called after the
     * specified number of seconds.
     */
    private void scheduleNextTask(int seconds)
    {
        timer.schedule(new NextActionTask(), seconds * 1000);
    }

    /**
     * Schedules the next game state update if the conditions within
     * {@link EndDrawTask#run()} are met, the method will be called after
     * drawTimeLimit seconds.
     */
    private void scheduleEndDrawTask()
    {

        timer.schedule(new EndDrawTask(), drawTimeLimit * 1000);
    }

    /**
     * Sets the status to {@link PictionaryGame#TURN_END} and lets the next
     * player draw.
     */
    private void turnFinished()
    {
        status = TURN_END;
        currentPlayer = pictionaryPlayers.get(pictionaryPlayers
                .indexOf(currentPlayer) + 1);
        System.out.println("turn ended, letting next player draw");
    }

    /**
     * Sets status to {@link PictionaryGame#ROUND_FINISHED} and reports the
     * scores to all the connected players.
     */
    private void roundFinished()
    {
        System.out.println("Round finished");
        status = ROUND_FINISHED;
        List<String> players = new ArrayList<String>();
        List<Integer> scores = new ArrayList<Integer>();
        for (PictionaryPlayer player : pictionaryPlayers)
        {
            players.add(player.getClientID());
            scores.add(player.getScore());
        }

        pictionaryPlayers.addAll(newPlayers);
        for (PictionaryPlayer player : pictionaryPlayers)
        {
            player.reportScores(players, scores);
        }
        newPlayers.clear();
    }

    /**
     * This method is called by the hosting server when it gets a join request
     * from a client. <br />
     * It either lets the new player start playing at once or makes her wait
     * until the current round has finished, depending on the game's state.
     * 
     * @param player    The player wanting to join.
     */
    public void join(PictionaryPlayer player)
    {
        if (status == GAME_STARTED || status == TURN_END)
        {
            player.setStatus(PictionaryPlayer.WAITING_FOR_TURN);
            newPlayers.add(player);
        }
        else
        {
            player.setStatus(PictionaryPlayer.WAITING_FOR_TURN);
            pictionaryPlayers.add(player);
            if (status == NOT_ENOUGH_PLAYERS_TO_START
                    || status == NOT_ENOUGH_PLAYERS_TO_CONTINUE)
            {
                startNewGame();
            }
        }
    }

    /**
     * Called by the hosting server when it gets a disconnect request, calling
     * this method informs the game that a player wants to leave. <br />
     * The game then removes the player, takes appropriate action to ensure that
     * the game continues and informs all the players that someone left the
     * game.
     * 
     * @param player    The player to be removed from the game.
     */
    public void playerLeft(PictionaryPlayer player)
    {
        pictionaryPlayers.remove(player);
        if(player.getStatus() == PictionaryPlayer.DRAWING)
        {
            timer.cancel();
            timer = new Timer();
            skipEndDrawTasks = 0;
            nextAction();
        }
        else if(pictionaryPlayers.size() == 1)
        {
            nextAction();
        }
    }

    /**
     * This method is used to check whether a player is in the game or not.
     * 
     * @param player    The player to look for.
     * @return          True if the player is found, else false.
     */
    public boolean playerExists(PictionaryPlayer player)
    {
        return (pictionaryPlayers.contains(player));
    }

    /**
     * This method is called when a player wants to make a guess.
     * 
     * @param guessingPlayer    The guessing player.
     * @param word              The word that the player guesses.
     */
    public void guessWord(PictionaryPlayer guessingPlayer, String word)
    {
        if (status != GAME_STARTED)
        {
            System.err
                    .println("Error: a player guessed when the game wasn't running");
            return;
        }
        if (!pictionaryPlayers.contains(guessingPlayer))
        {
            return;
        }
        else if (guessingPlayer.getStatus() == PictionaryPlayer.DRAWING)
        {
            return;
        }
        if (word.equalsIgnoreCase(currentWord))
        {
            correctGuess = true;
            currentPlayer.setScore(currentPlayer.getScore() + 1);
            guessingPlayer.setScore(guessingPlayer.getScore() + 2);
            guessingPlayer.correctGuess(currentWord);
            for (PictionaryPlayer player : pictionaryPlayers)
            {
                if (player.equals(guessingPlayer))
                {
                    continue;
                }
                player.correctGuessBroadcast(guessingPlayer.getClientID(),
                        currentWord);
            }
            nextAction();
            skipEndDrawTasks += 1;
        }
        else
        {
            for (PictionaryPlayer player : pictionaryPlayers)
            {
                player.wrongGuess(guessingPlayer.getClientID(), word);
            }
        }
    }

    /**
     * Gets a collection of all the player names.
     * 
     * @return A {@link java.util.List} of String containing the names of all
     *         the players.
     */
    public List<String> getPlayerNames()
    {
        List<String> players = new ArrayList<String>();
        for (PictionaryPlayer pp : pictionaryPlayers)
        {
            players.add(pp.getClientID());
        }
        for (PictionaryPlayer pp : newPlayers)
        {
            players.add(pp.getClientID());
        }
        return players;
    }

}
