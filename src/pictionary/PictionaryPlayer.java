package pictionary;

import java.util.List;

/**
 * This abstract class serves as a foundation for a Pictionary player. <br />
 * It has methods which enables a {@link PictionaryGame} to interact with it.
 * 
 * @author      Kristoffer Nordkvist
 */
public abstract class PictionaryPlayer
{
    /**
     * The player is waiting for his turn.
     */
    public final static int WAITING_FOR_TURN = 0;

    /**
     * The player is drawing.
     */
    public final static int DRAWING = 1;

    /**
     * The playing is in the game, but not yet playing.
     */
    public final static int SPECTATING = 2;

    /**
     * The player's score.
     */
    private int score;

    /**
     * The players current status. <br />
     * The value equals one of the static status integers, for example
     * {@link PictionaryPlayer#WAITING_FOR_TURN}.
     */
    private int status;

    /**
     * The constructor.
     * 
     * @param score     The player's score.
     * @param status    The player's status.
     */
    public PictionaryPlayer(int score, int status)
    {
        this.score = score;
        this.status = status;
    }

    /**
     * This method uses String's {@link String#equals(Object)} to compare the
     * value of this player's {@link PictionaryPlayer#getClientID()} with the
     * other player's getClientID().
     * 
     */
    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof PictionaryPlayer))
        {
            return false;
        }
        PictionaryPlayer pp = (PictionaryPlayer) obj;
        return this.getClientID().equals(pp.getClientID());
    }

    /**
     * Set this player's score.
     * 
     * @param score     The new score.
     */
    public void setScore(int score)
    {
        this.score = score;
    }

    /**
     * Sets this player's status.
     * 
     * @param status    The new status.
     */
    public void setStatus(int status)
    {
        this.status = status;
    }

    /**
     * Gets the players score.
     * 
     * @return  The player's score.
     */
    public int getScore()
    {
        return score;
    }

    /**
     * Gets the players status.
     * 
     * @return  The player's status.
     */
    public int getStatus()
    {
        return status;
    }

    /**
     * Informs the player that a player stopped drawing.
     * 
     * @param player    The name of the player that stopped drawing.
     */
    public abstract void playerStoppedDrawing(String player);

    /**
     * Informs the player that its draw turn is over.
     */
    public abstract void stopDraw();

    /**
     * Informs the player that a player started drawing.
     * 
     * @param player    The name of the player that started drawing.
     */
    public abstract void playerStartedDrawing(String player);

    /**
     * Informs the player that it is her turn to start drawing a word.
     * 
     * @param wordToDraw        The word that the player should attempt to draw.
     */
    public abstract void startDraw(String wordToDraw);

    /**
     * Informs the player that it can start guessing on the word being drawn.
     */
    public abstract void startGuessing();

    /**
     * Informs the player that a new game of Pictionary has started.
     */
    public abstract void gameStarted();

    /**
     * Informs the player that he made a correct guess.
     * 
     * @param guess     The guess that was correct.
     */
    public abstract void correctGuess(String guess);

    /**
     * Informs the player that a player made a correct guess.
     * 
     * @param guessingPlayer    The player that made the guess.
     * @param word      The correct word.
     */
    public abstract void correctGuessBroadcast(String guessingPlayer,
            String word);

    /**
     * Gets the player's ID.
     * 
     * @return  The player's ID.
     */
    public abstract String getClientID();

    /**
     * Reports the players' scores. <br />
     * A score at a specific index in <code>scores</code> corresponds to the
     * player at the same index in <code>players</code>.
     * 
     * @param players   A collection of players.
     * @param scores    A collection of scores.
     */
    public abstract void reportScores(List<String> players, List<Integer> scores);

    /**
     * Informs the player that the guessing turn is over.
     */
    public abstract void stopGuessing();

    /**
     * Informs the player that someone made an incorrect guess.
     * 
     * @param player    The player that made the guess.
     * @param guess     The incorrect guess.
     */
    public abstract void wrongGuess(String player, String guess);

    /**
     * Tells the player the last word. This method is used when no one makes a
     * correct guess.
     * 
     * @param currentWord       The word that was being drawn.
     */
    public abstract void tellWord(String currentWord);
}