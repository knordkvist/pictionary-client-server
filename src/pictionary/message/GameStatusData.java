package pictionary.message;
import java.io.Serializable;
import java.util.List;

/**
 * A simple class containing information about a {@link pictionary.PictionaryGame}. <br />
 * @author      Kristoffer Nordkvist
 */
public class GameStatusData implements Serializable
{
    /**
     * All the players participating in the Pictionary game.
     */
    public final List<String> connectedPlayers;
    /**
     * The game's status, equal to one of the status integers in {@link pictionary.PictionaryGame},
     * for example {@link pictionary.PictionaryGame#GAME_STARTED}.
     */
    public final int gameStatus;
    
    /**
     * The constructor.
     * @param connectedPlayers  The {@link GameStatusData#connectedPlayers}.
     * @param gameStatus        The {@link GameStatusData#gameStatus}.
     */
    public GameStatusData(List<String> connectedPlayers, int gameStatus)
    {
        this.connectedPlayers = connectedPlayers;
        this.gameStatus = gameStatus;
    }
}
