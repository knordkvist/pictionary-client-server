package pictionary.message;

import java.awt.Point;
import java.io.Serializable;
import java.util.EnumSet;
import java.util.Hashtable;

/**
 * This class defines the package of data that is sent between the server and
 * clients. <br />
 * Messages can contain data or not.
 * If they do contain data, for example a chat message from a client, it is stored in {@link Message#messageData}.
 * 
 * @author      Kristoffer Nordkvist
 */
public final class Message implements Serializable
{
    /**
     * Used in {@link Message} to determine the type of message.
     */
    public enum MessageType
    {
        /**
         * Indicates that the server is closing.
         */
        ServerDisconnecting,

        /**
         * A message from the server.
         */
        ServerMessage,
        
        /**
         * A message broadcast from the specified client.
         */
        ClientMessageBroadcast,
        
        /**
         * Tells a client it can start guessing.
         */
        StartGuessing,
        
        /**
         * The specified client stopped drawing.
         */
        AClientStoppedDrawing,
        
        /**
         * Tells a client to start drawing.
         */
        StartDrawing,
        
        /**
         * Tells a client to stop drawing.
         */
        StopDrawing,
        
        /**
         * Tells a client to stop guessing.
         */
        StopGuessing,

        /**
         * Informs a client that he made a correct guess.
         */
        CorrectGuess,
        
        /**
         * Sent from a client to tell the server that it wants to join
         * a game of Pictionary.
         */
        JoinGameRequest,
        
        /**
         * Informs the client that is should send a message
         * containing an username.
         */
        ServerRequestUsername,
        
        /**
         * Informs the client that the server has accepted
         * the connection.
         */
        ServerAcceptedConnection,
        
        /**
         * Sent as a response to {@link MessageType#ServerRequestUsername}.
         */
        ClientUsernameResponse,
        
        /**
         * Sent to clients to inform them that someone
         * made a correct guess.
         */
        AClientCorrectGuess,
        
        /**
         * Sent to clients to inform them that someone
         * made a faulty guess.
         */
        AClientIncorrectGuess,
        
        /**
         * This message contains draw data.
         */
        DrawData,
        
        /**
         * Sent from a client when it wants to disconnect from the server.
         */
        DisconnectRequest,
        
        /**
         * Sent to a client to inform it that a client disconnected.
         */
        AClientDisconnected,
        
        /**
         * Sent to a client to inform it that a client started drawing.
         */
        AClientStartedDrawing,
        
        /**
         * Sent to a client to inform it that a client connected to the server.
         */
        AClientConnectedToServer,
        
        /**
         * Send to a client to inform it
         * that a new player joined the game.
         */
        AClientJoinedGame,
        
        /**
         * Sent to the client when it joins a new game.
         */
        JoinGameMessage,
        
        /**
         * Sent from a client when it makes a guess.
         */
        Guess
    }

    /**
     * HashTable which is used to check if a combination of data type and message type is legal. <br />
     * For example: getting the values for the key String.class could return an EnumSet containing
     * the legal types ServerMessage and ClientMessageBroadcast. <br />
     * This table is used by {@link Message#legalCombination(MessageType, Class)}.
     */
    // Table: synchronized, Map: not synchronized.
    private static final Hashtable<Class<?>, EnumSet<MessageType>> legalCombinations = new Hashtable<Class<?>, EnumSet<MessageType>>();
    
    /**
     * Contains all the message types that can contain string message data.
     */
    private static final EnumSet<MessageType> stringDataMessages = EnumSet.of(
            MessageType.ServerMessage,
            MessageType.ClientMessageBroadcast,
            MessageType.AClientIncorrectGuess,
            MessageType.AClientCorrectGuess,
            MessageType.Guess,
            MessageType.CorrectGuess);
    
    /**
     * Contains all the message types that can contain start draw data.
     */
    private static final EnumSet<MessageType> startDrawDataMessages =
        EnumSet.of(MessageType.StartDrawing);
    
    /**
     * Contains all the message types that can contain status message data.
     */
    private static final EnumSet<MessageType> statusMessages =
        EnumSet.of(MessageType.ServerAcceptedConnection);
    
    /**
     * Contains all the message types that can contain game status data.
     */
    private static final EnumSet<MessageType> gameStatusMessages =
        EnumSet.of(MessageType.JoinGameMessage, MessageType.ServerAcceptedConnection);
    
    /**
     * Contains all the message types that can contain draw data message.
     */
    private static final EnumSet<MessageType> drawDataMessages = EnumSet.of(MessageType.DrawData);
    
    static
    {
        legalCombinations.put(String.class, stringDataMessages);
        legalCombinations.put(StartDrawData.class, startDrawDataMessages);
        legalCombinations.put(StatusData.class, statusMessages);
        legalCombinations.put(GameStatusData.class, gameStatusMessages);
        legalCombinations.put(Point.class, drawDataMessages);
    }
    
    /**
     * The type of message.
     */
    public final MessageType messageType;
    
    /**
     * The client. <br />
     * This variable can have different meanings depending on the type of message,
     * for example the recipient ID or the sender ID.
     */
    public final String client;
    
    /**
     * The data associated with this message,
     * is null with some message types where data is not needed;
     * for example {@link MessageType#StopGuessing} where the message type alone conveys enough information. <br />
     * See {@link MessageData} for details.
     */
    public final MessageData<?> messageData;
    
    /**
     * Constructor for the simplest message. <br />
     * {@link Message#messageType} is set, but both {@link Message#client} and {@link Message#messageData} is null. <br />
     * Performs checking to ensure that this a message of this type should not contain data.
     * 
     * @param messageType       The message's {@link MessageType}.
     */
    public Message(MessageType messageType)
    {
        this.messageType = messageType;
        this.client = null;
        this.messageData = null;
    }
    
    /**
     * Constructor for a basic message without message data. <br />
     * Both {@link messageType} and {@link client} is set,
     * but {@link messageData} is null. <br />
     * 
     * @param messageType       The {@link MessageType}.
     * @param client    The {@link client}.
     * @throws IllegalArgumentException If messages of this message type should contain data,
     * uses {@link Message#shouldContainData(MessageType)} to perform this check.
     */
    public Message(MessageType messageType, String client) throws IllegalArgumentException
    {
        this.messageType = messageType;
        this.client = client;
        this.messageData = null;
        if(shouldContainData(messageType))
        {
            throw new IllegalArgumentException(
                    "Messages of the type " + messageType.toString() + " are expected to contain data.");
        }
    }
    
    /**
     * Creates a Message with the specified type, client and data.
     * 
     * @param messageType       The message type.
     * @param client            The client, see {@link Message#client}.
     * @param messageData       The data contained in this message, see {@link Message#messageData}
     * @throws IllegalArgumentException If the combination of message type and message data is illegal,
     * uses {@link Message#legalCombination(MessageType, Class)} to perform this check.
     */
    private Message(MessageType messageType, String client,
            MessageData<?> messageData) throws IllegalArgumentException
    {
        if (!legalCombination(messageType, messageData.data.getClass()))
        {
            throw new IllegalArgumentException(
                    "The combination of messageType " + messageType
                            + "and data type " + messageData.data.getClass()
                            + " was not legal");
        }
        this.messageType = messageType;
        this.client = client;
        this.messageData = messageData;
    }

    /**
     * Creates and returns a Message object containing data. <br />
     * This factory method has built in checks to assure that the message type and the type of data
     * is consistent. <br />
     * This means that, for example, no one can create a Message
     * with the MessageType of {@link MessageType#ClientDrawData} and a data object that
     * is anything else but {@link java.awt.Point}. <br />
     * Wraps {@link Message#Message(MessageType, String, MessageData)}.
     * @param <T>               The type of data.
     * @param messageType       he type of message.
     * @param client            The client, see  {@link Message#client}.
     * @param data              This message's message data, see {@link Message#messageData}.
     * @return                  An instance of the class Message,
     *                          or null if an IllegalArgumentException is caught.
     */
    public static <T> Message CreateDataMessage(MessageType messageType,
            String client, T data)
    {
        Message message = null;
        try
        {
            message =  new Message(messageType, client, new MessageData<T>(data));
        }
        catch(IllegalArgumentException e)
        {
            e.printStackTrace();
        }
        return message;
    }
    
    /**
     * Convenience method.
     * Passes the arguments along to {@link Message#CreateDataMessage(MessageType, String, Object)},
     * using null for the String object.
     * 
     * @param messageType       See {@link Message#CreateDataMessage(MessageType, String, Object)}
     * @param data              See {@link Message#CreateDataMessage(MessageType, String, Object)}.
     * @see                     Message#CreateDataMessage(MessageType, String, Object)
     */
    public static <T> Message CreateDataMessage(MessageType messageType, T data)
    {
        return CreateDataMessage(messageType, null, data);
    }
    
    /**
     * Determines whether a combination of message type and data is legal or not.
     * 
     * @param messageType       The {@link MessageType}.
     * @param klass             The {@link Class}.
     * @return                  True if {@link Message#legalCombinations} containsan EnumSet
     *                          which in turn contains the specified message type, else false.
     */
    private static boolean legalCombination(MessageType messageType, Class<?> klass)
    {
        if (!legalCombinations.containsKey(klass))
        {
            return false;
        }
        EnumSet<MessageType> messageTypeEnumSet = legalCombinations.get(klass);
        return messageTypeEnumSet.contains(messageType);
    }
    
    /**
     * Determines whether a message of the specified {@link Message#messageType} should contain data or not. <br />
     * Iterates through the {@link EnumSet}s contained in {@link Message#legalCombinations},
     * looking for the specified message type.
     * 
     * @param messageType       The {@link Message#messageType}.
     * @return                  True if the {@link Message#messageType} was found in any of the {@link EnumSet}s,
     *                          else false.
     */
    private static boolean shouldContainData(MessageType messageType)
    {
        for(EnumSet<MessageType> enumSet : legalCombinations.values())
        {
            if(enumSet.contains(messageType))
            {
                /*
                 * This messageType should always contain some sort of data, therefore we return true.
                 */
                return true;
            }
        }
        /*
         * This message type is not associated with message data, return false.
         */
        return false;
    }
}
