package pictionary.message;

import java.io.Serializable;

/**
 * A generic class wrapping an object with the same type as the type parameter. <br />
 * Every {@link Message} contains an instance of this class.
 * 
 * @author      Kristoffer Nordkvist
 *
 * @param <T>   {@link MessageData#data}'s type.
 */
public final class MessageData<T> implements Serializable
{
    /**
     * The data object.
     */
    public final T data;
    
    /**
     * The constructor.
     * @param data      The {@link MessageData#data}.
     */
    protected MessageData(T data)
    {
        this.data = data;
    }
}