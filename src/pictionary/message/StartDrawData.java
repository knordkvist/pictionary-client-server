package pictionary.message;

import java.io.Serializable;

/**
 * The data contained in the {@link MessageData} of a {@link Message}
 * sent to a player when it is the player's time to draw.
 * 
 * @author      Kristoffer Nordkvist
 */
public class StartDrawData implements Serializable
{
    /**
     * The word the player should attempt to draw.
     */
    public final String theWord;
    
    /**
     * The amount of time, in seconds, the player can draw before the time is up.
     */
    public final int timeLimit;
    
    /**
     * The constructor.
     * @param theWord   The word to draw, see {@link theWord}.
     * @param timeLimit The time limit, see {@link timeLimit}.
     */
    public StartDrawData(String theWord, int timeLimit)
    {
        this.theWord = theWord;
        this.timeLimit = timeLimit;
    }
}
