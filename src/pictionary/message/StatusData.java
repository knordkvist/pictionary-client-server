package pictionary.message;
import java.io.Serializable;
import java.util.List;

/**
 * Contains information about connected clients,
 * used in the {@link MessageData} of certain {@link Message}s.
 * 
 * @author Kristoffer Nordkvist
 */
public class StatusData implements Serializable
{
    /**
     * A {@link List} containing the names of the connected clients.
     */
    public final List<String> connectedClients;
    
    /**
     * The constructor.
     * 
     * @param connectedClients  The connected clients.
     */
    public StatusData(List<String> connectedClients)
    {
        this.connectedClients = connectedClients;
    }
}
