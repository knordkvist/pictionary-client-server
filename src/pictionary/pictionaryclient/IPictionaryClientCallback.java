package pictionary.pictionaryclient;

import java.util.List;
import java.awt.Point;

/**
 * An interface defining the methods which an pictionary client should implement,
 * these methods are called by {@link PictionaryClient}
 * and implemented by {@link PictionaryClientGUI}.
 * 
 * @author      Kristoffer Nordkvist
 */
public interface IPictionaryClientCallback
{
    /**
     * This method is called when a message from the server sent to all the clients is received.
     * 
     * @param message   The {@link pictionary.message.Message} sent from the server.
     */
    public void serverMessageReceived(String message);
    
    /**
     * This method is called when a message from a client, broadcasted to all other clients, is received.
     * 
     * @param client    The name of the client that sent the message.
     * @param message   The {@link pictionary.message.Message} sent from <code>
     */
    public void clientBroadcastMessageReceived(String client, String message);
    
    /**
     * This method is called when a new client has connected to the server.
     * 
     * @param client    The name of the new client that connected.
     */
    public void aClientConnected(String client);
    
    /**
     * This method is called when a client has disconnected from the server.
     * 
     * @param client    The name of the client that disconnected.
     */
    public void aClientDisconnected(String client);
    
    /**
     * This method is called when the client successfully joined a game of Pictionary.
     * 
     * @param players           All the players participating in the game.
     * @param gameStatus        The status of the game, equal to a status int in {@link pictionary.PictionaryGame}.
     */
    public void joinedNewGame(List<String> players, int gameStatus);
    
    /**
     * This method is called when the player should start drawing a word.
     * 
     * @param wordToDraw        The word that the player should attempt to draw.
     * @param timeLimit         The amount of time, in seconds, the player can draw before his time is up.
     */
    public void startDrawing(String wordToDraw, int timeLimit);
    
    /**
     * This method is called when the connection to the server is lost.
     */
    public void serverDisconnected();
    
    /**
     * This method is called to notify the client that the drawing player drew at a specific point.
     * 
     * @param point     A {@link Point} containing the coordinates that were filled.
     */
    public void drawReceived(Point point);
    
    /**
     * This method is called to notify the client that it should stop drawing. <br />
     * This could happen when the draw time is up or when a player made a correct guess.
     */
    public void stopDrawing();
    
    /**
     * This method is called to notify the client that a player is drawing
     * and that the client can start making guesses.
     */
    public void startGuessing();
    
    /**
     * This method is called to notify the client that it should stop guessing,
     * for example when someone made a correct guess or when the drawing player's time is up.
     */
    public void stopGuessing();
    
    /**
     * This method is called to notify the client that a player made a correct guess.
     * 
     * @param player    The name of the player that made the correct guess.
     * @param guess     The guess that was made.
     */
    public void correctGuessBroadcastReceived(String player, String guess);
    
    /**
     * This method is called when the player has made a correct guess.
     */
    public void correctGuess();
    
    /**
     * This method notifies the client that a player made an incorrect guess.
     * 
     * @param player    The name of the player that made the incorrect guess.
     * @param guess     The incorrect guess.
     */
    public void wrongGuessBroadcastReceived(String player, String guess);
    
    /**
     * This method is called when the player successfully connected to a server.
     */
    public void connectedToServer();
    
    /**
     * This method is called when the server has requested the client to respond with an username. <br />
     * This method will be called until the server has accepted the username.
     * 
     * @return  Returns The username that will identify this client,
     *          and will be seen by the other connected clients.
     */
    public String userNameRequest();
    
    /**
     * This method is called when a client has started drawing.
     * 
     * @param client    The name of the client that started drawing.
     */
    public void clientStartedDrawing(String client);
    
    /**
     * This method is called when the connection to the server has been closed.
     * 
     * @param error     A description of the cause to the error.
     */
    public void closedByError(String error);
}
