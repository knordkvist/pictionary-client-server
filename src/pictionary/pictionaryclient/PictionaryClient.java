package pictionary.pictionaryclient;

import pictionary.message.*;
import pictionary.message.Message.MessageType;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.awt.Point;

/**
 * Implementation of a basic Pictionary Client
 * which handles communication with a {@link pictionary.pictionaryserver.PictionaryServer}. <br />
 * Uses the private class {@link ServerConnection} for the actual communication with the server. <br />
 * PictionaryClient's job is to handle communication with the server and just forward information to
 * an {@link IPictionaryClientCallback}.
 * 
 * @author Kristoffer Nordkvist
 */
public final class PictionaryClient
{
    /**
     * A collection containing the connected client's unique names.
     */
    public List<String> connectedClients; // TODO visa personer + deras status i ett separat GUI-element
    
    /**
     * Represents the network connection to the server.
     */
    private ServerConnection connection = null;
    
    /**
     * The server's host name.
     */
    private String serverHostName;
    
    /**
     * The server's port.
     */
    private int serverPort;
    
    /**
     * The {@link IPictionaryClientCallback}, in this case {@link PictionaryClientGUI}.
     */
    private IPictionaryClientCallback callback = null;
    
    /**
     * The constructor.
     * 
     * @param serverHostName    The server's host name, this is used when connecting to the server.
     * @param serverPort        The server's port, this is used when connecting to the server.
     */
    public PictionaryClient(String serverHostName, int serverPort)
    {
        this.serverHostName = serverHostName;
        this.serverPort = serverPort;
    }
    
    /**
     * Creates a new {@link ServerConnection} using {@link #serverHostName} and {@link #serverPort}.
     * 
     * @param callback          The {@link IPictionaryClientCallback} which will be notified of events.
     * @throws IOException      If the connection to the server could not be established.      
     */
    public void connect(IPictionaryClientCallback callback)
            throws IOException
    {
        this.callback = callback;
        connection = new ServerConnection(serverHostName, serverPort);
    }
    
    /**
     * Sends a message to the server, informing it that we want to join a game of Pictionary.
     */
    public void joinGame()
    {
        send(new Message(MessageType.JoinGameRequest));
    }
    
    /**
     * Sends a message to the server, informing it that we are about to disconnect from it.
     */
    public void disconnect()
    {
        if (connection != null)
        {
            connection.send(new Message(MessageType.DisconnectRequest));
        }
    }
    
    /**
     * Sends a {@link Message} to the server using {@link ServerConnection#send(Message)}.
     * 
     * @param message                   The message to be sent to the server.
     * @throws IllegalStateException    If {@link #connection}'s boolean {@link ServerConnection#closed} equals true.
     */
    private void send(Message message) throws IllegalStateException
    {
        if (connection.closed)
        {
            throw new IllegalStateException(
                    "Can't send messages when the connection is closed.");
        }
        connection.send(message);
    }
    
    /**
     * Creates and sends a {@link Message} of the type {@link MessageType#ClientMessageBroadcast}
     * using {@link ServerConnection#send(Message)}.
     *  
     * @param message   The Message to be sent.
     */
    public void broadcastChatMessage(String message)
    {
        send(Message.CreateDataMessage(Message.MessageType.ClientMessageBroadcast, this.getID(), message));
    }
    
    /**
     * Creates and sends a {@link Message} of the type {@link MessageType#Guess}
     * using {@link ServerConnection#send(Message)}.
     * @param guess     The word that was guessed by the player.
     */
    public void guess(String guess)
    {
        if(guess != "")
        {
            send(Message.CreateDataMessage(MessageType.Guess, guess));
        }
    }
    
    /**
     * Creates and sends a {@link Message} of the type {@link MessageType#DrawData}
     * using {@link ServerConnection#send(Message)}.
     * @param p The {@link Point} to send to the server.
     */
    public void broadcastDraw(Point p)
    {
        send(Message.CreateDataMessage(MessageType.DrawData, p));
    }
    
    /**
     * Gets this client unique ID, used by the server to identify it.
     * @return {@link ServerConnection#id}.
     */
    public final String getID()
    {
        return connection.id;
    }
    
    /**
     * Gets information about the server's hostname and port, this method is used by {@link PictionaryClientGUI}
     * when setting the client's window title.
     * 
     * @return  A String in the format of "Connected to [hostname] at port [port]". 
     */
    public final String getServerInfo()
    {
        return "Connected to " + connection.socket.getInetAddress().toString()
                + " at port " + connection.socket.getPort();
    }

    /**
     * ServerConnection handles communication with the {@link pictionary.pictionaryserver.PictionaryServer}.<br />
     * It uses two threads for sending and receiving messages; {@link SendThread} and {@link ReceiveThread}.
     */
    private class ServerConnection
    {
        /**
         * The clients ID.
         */
        private final String id;
        
        /**
         * The socket connected to the server.
         */
        private final Socket socket;
        
        /**
         * The stream used for reading messages from the server.
         */
        private final ObjectInputStream in;
        
        /**
         * The stream used for sending messages to the server.
         */
        private final ObjectOutputStream out;
        
        /**
         * The thread that sends messages to the server.
         */
        private final SendThread sendThread;
        
        /**
         * The thread that received messages from the server.
         */
        private final ReceiveThread receiveThread;
        
        /**
         * Messages waiting to be sent to the server by {@link #sendThread}.
         */
        private final LinkedBlockingQueue<Message> outgoingMessages;
        
        /**
         * When the connection is closing, closed is set to true.
         */
        private volatile boolean closed;
        
        /**
         * The constructor.
         * 
         * @param host          The server's hostname.
         * @param port          The server's port.
         * @throws IOException  if the {@link ServerConnection#socket} could not
         *                      be created and connected to the server.
         */
        ServerConnection(String host, int port) throws IOException
        {
            outgoingMessages = new LinkedBlockingQueue<Message>();
            socket = new Socket(host, port);
            out = new ObjectOutputStream(socket.getOutputStream());
            in = new ObjectInputStream(socket.getInputStream());
            
            String userName = null;
            try
            {
                Message message = (Message)in.readObject();
                // Be användaren om ett användarnamn, skicka detta till servern,
                // upprepa tills servern accepterar användarnamnet.
                while(message.messageType == MessageType.ServerRequestUsername)
                {
                    userName = callback.userNameRequest().trim();
                    out.writeObject(
                            new Message(MessageType.ClientUsernameResponse, userName));
                    out.flush();
                    message = (Message)in.readObject();
                }
                // Ta hand om det extra meddelandet.
                this.handleMessage(message);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                closedByError("Could not connect to the server.");
                id = null;
                sendThread = null;
                receiveThread = null;
                return;
            }
            id = userName;
            sendThread = new SendThread();
            receiveThread = new ReceiveThread();
            sendThread.start();
            receiveThread.start();
        }
        
        /**
         * Closes the connection.
         */
        private void close()
        {
            closed = true;
            if(sendThread != null && receiveThread != null)
            {
                sendThread.interrupt();
                receiveThread.interrupt();
            }
            try
            {
                socket.close();
            }
            catch (IOException e)
            {
            }
        }
        
        /**
         * Simply adds a message to {@link #outgoingMessages}.
         * 
         * @param message The {@link Message} to be sent to the server.
         */
        private void send(Message message)
        {
            outgoingMessages.add(message);
        }
        
        /**
         * Called when an error occurs, probably in {@link #receiveThread} or {@link #sendThread}.<br />
         * Also passes the information along to
         * {@link #callback}'s {@link IPictionaryClientCallback#closedByError(String)}.
         * 
         * @param error A string describing the error.
         */
        private synchronized void closedByError(String error)
        {
            callback.closedByError(error);
            System.err.println(error);
            if (!closed)
            {
                close();
            }
        }
        
        /**
         * This method is called when a {@link Message} has been received in {@link ReceiveThread}.<br />
         * It checks the message's {@link Message#messageType} and takes appropriate action,
         * for example calling {@link PictionaryClient#callback}'s
         * {@link IPictionaryClientCallback#connectedToServer()} method when a message of the type
         * {@link MessageType#ServerAcceptedConnection} is received.
         * 
         * @param message The Message that was received from the server.
         */
        private void handleMessage(Message message)
        {
            if(message.messageType == MessageType.ServerAcceptedConnection)
            {
                StatusData statusData = (StatusData)message.messageData.data;
                connectedClients = statusData.connectedClients;
                callback.connectedToServer();
            }
            else if(message.messageType == MessageType.AClientConnectedToServer)
            {
                connectedClients.add(message.client);
                callback.aClientConnected(message.client); 
            }
            else if(message.messageType == MessageType.AClientDisconnected)
            {
                connectedClients.remove(message.client);
                callback.aClientDisconnected(message.client);
            }
            else if(message.messageType == MessageType.ServerMessage)
            {
                callback.serverMessageReceived((String)message.messageData.data);
            }
            else if(message.messageType == MessageType.ClientMessageBroadcast)
            {
                callback.clientBroadcastMessageReceived(message.client, (String)message.messageData.data);
            }
            else if(message.messageType == MessageType.JoinGameMessage)
            {
                GameStatusData gameStatus = (GameStatusData)message.messageData.data;
                callback.joinedNewGame(gameStatus.connectedPlayers, gameStatus.gameStatus);
            }
            else if (message.messageType == MessageType.StartDrawing)
            {
                StartDrawData startDrawData = (StartDrawData) message.messageData.data;
                callback.startDrawing(startDrawData.theWord, startDrawData.timeLimit);
            }
            else if (message.messageType == MessageType.StopDrawing)
            {
                callback.stopDrawing();
            }
            else if (message.messageType == MessageType.DrawData)
            {
                Point point = (Point)message.messageData.data; 
                callback.drawReceived(point);
            }
            else if (message.messageType == MessageType.StartGuessing)
            {
                callback.startGuessing();
            }
            else if (message.messageType == MessageType.StopGuessing)
            {
                callback.stopGuessing();
            }
            else if (message.messageType == MessageType.CorrectGuess)
            {
                callback.correctGuess();
            }
            else if (message.messageType == MessageType.AClientCorrectGuess)
            {
                callback.correctGuessBroadcastReceived(
                        message.client, (String) message.messageData.data);
            }
            else if (message.messageType == MessageType.AClientIncorrectGuess)
            {
                callback.wrongGuessBroadcastReceived(message.client, (String)message.messageData.data);
            }
            else if(message.messageType == MessageType.AClientStartedDrawing)
            {
                callback.clientStartedDrawing(message.client);
            }
        }
        
        /**
         * Responsible for taking {@link Message}s from {@link ServerConnection#outgoingMessages}
         * and sending them to the server using {@link ServerConnection#out}.
         */
        private class SendThread extends Thread
        {
            @Override
            public void run()
            {
                System.out.println("SendThread running.");
                try
                {
                    while (!closed)
                    {
                        Message message = outgoingMessages.take();
                        out.reset();
                        out.writeObject(message);
                        out.flush();
                        if(message.messageType == MessageType.DisconnectRequest)
                        {
                            connection.close();
                        }
                    }
                }
                catch (IOException e)
                {
                    if (!closed)
                    {
                        System.out.println("SendThread killed by IOException: " + e);
                        closedByError("Error occured when sending message.");
                    }
                }
                catch (Exception e)
                {
                    if (!closed)
                    {
                        System.out.println("Chaos in SendThread:\n");
                        e.printStackTrace();
                        closedByError("SendThread encountered an unexpected error: " + e);
                    }
                }
                finally
                {
                    System.out.println("SendThread terminated.");
                }
            }
        }

        /**
         * Responsible for reading {@link Message}s from {@link ServerConnection#in}
         * and pass them along to {@link ServerConnection#handleMessage}.<br />
         * Will close the connection if a Message of the type {@link MessageType#ServerDisconnecting} is read.
         */
        private class ReceiveThread extends Thread
        {
            @Override
            public void run()
            {
                System.out.println("ReceiveThread running.");
                try
                {
                    while (!closed)
                    {
                        Message message = (Message) in.readObject();
                        if (message.messageType == MessageType.ServerDisconnecting)
                        {
                            callback.serverDisconnected();
                            close();
                        }
                        else
                        {
                            handleMessage(message);
                        }
                    }
                }
                catch (IOException e)
                {
                    if (!closed)
                    {
                        System.out.println("ReceiveThread killed by IOException: " + e);
                        closedByError("Error occured when receiving message.");
                    }
                }
                catch (Exception e)
                {
                    if (!closed)
                    {
                        System.out.println("Chaos in ReceiveThread:\n");
                        e.printStackTrace();
                        closedByError("ReceiveThread encountered an unexpected error: " + e);
                    }
                }
                finally
                {
                    System.out.println("ReceiveThread terminated.");
                }
            }
        }

    }
}
