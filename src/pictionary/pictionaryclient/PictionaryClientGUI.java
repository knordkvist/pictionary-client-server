package pictionary.pictionaryclient;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.swing.*;
import javax.swing.border.*;

import pictionary.PictionaryGame;

import java.awt.event.*;
import java.awt.*;

/**
 * A simple Pictionary GUI. <br />
 * This class receives and forwards information to a {@link PictionaryClient}
 * @author Kristoffer Nordkvist
 */
public class PictionaryClientGUI extends JFrame implements
        IPictionaryClientCallback
{
    /**
     * The {@link PictionaryClient} that we receive and forward events from/to.
     */
    private PictionaryClient client;
    
    /**
     * The text area that contains received messages from the server and other clients.
     */
    private JTextArea receivedText = new JTextArea();
    
    /**
     * The area where chat text is input.
     */
    private JTextArea inputArea = new JTextArea();
    
    /**
     * The JScrollPane that {@link #receivedText} is attached to.
     */
    private JScrollPane receivedTextScrollPane;
    
    /**
     * The JScrollPane that {@link #inputArea} is attached to.
     */
    private JScrollPane inputAreaScrollPane;
    
    /**
     * The button used for connecting and disconnecting to/from the server,
     * is initially used for connecting.
     */
    private JButton disconnectOrConnectBtn = new JButton("Connect");
    
    /**
     * True if the client is connected to the server, else false.
     */
    private boolean connected = false;
    
    /**
     * The button used when joining or disconnecting from a game.
     */
    private JButton joinGameBtn = new JButton("Join game");
    
    /**
     * A button that can be used for sending the text in {@link #inputArea}.
     */
    private JButton sendTextBtn = new JButton("Send");
    
    /**
     * The field where the user inputs guesses when playing a game of pictionary.
     */
    private JTextField guessField = new JTextField();
    
    /**
     * The {@link Paper} on which the user can draw and also see what other players are drawing.
     */
    private Paper paper = new Paper();
    
    /**
     * The constructor. Sets up all initializes all the GUI elements.
     * @param client The {@link PictionaryClient}, see {@link #client}.
     */
    public PictionaryClientGUI(PictionaryClient client)
    {
        super("Not connected to a server");
        this.setLayout(new BorderLayout());
        this.addWindowListener(new CloseWindowListener());
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setSize(1024, 640);
        this.client = client;

        TitledBorder title;
        title = BorderFactory.createTitledBorder("Received text");
        receivedText.setLineWrap(true);
        receivedText.setBorder(title);
        title = BorderFactory.createTitledBorder("Write chat messages here");
        inputArea.setLineWrap(true);
        inputArea.setBorder(title);

        JPanel textAreaFrame = new JPanel();
        textAreaFrame.setLayout(new GridLayout(2, 1));
        receivedTextScrollPane = new JScrollPane(receivedText);
        receivedTextScrollPane.getVerticalScrollBar().addAdjustmentListener(
                new AdjustmentListener()
                {
                    public void adjustmentValueChanged(AdjustmentEvent e)
                    {
                        e.getAdjustable().setValue(
                                e.getAdjustable().getMaximum());
                    }
                });
        textAreaFrame.add(receivedTextScrollPane);

        inputAreaScrollPane = new JScrollPane(inputArea);
        inputAreaScrollPane.getVerticalScrollBar().addAdjustmentListener(
            new AdjustmentListener()
            {
                public void adjustmentValueChanged(AdjustmentEvent e)
                {
                    e.getAdjustable().setValue(
                            e.getAdjustable().getMaximum());
                }
            });
        inputAreaScrollPane.setPreferredSize(new Dimension(400, 100));
        textAreaFrame.add(inputAreaScrollPane);

        this.add(textAreaFrame, BorderLayout.LINE_END);

        JPanel buttonFrame = new JPanel();
        buttonFrame.setLayout(new BoxLayout(buttonFrame, BoxLayout.X_AXIS));
        buttonFrame.add(disconnectOrConnectBtn);
        buttonFrame.add(joinGameBtn);
        buttonFrame.add(sendTextBtn);
        buttonFrame.add(guessField);
        this.add(buttonFrame, BorderLayout.PAGE_END);

        receivedText.setEnabled(false);
        receivedText.setDisabledTextColor(Color.black);
        disconnectOrConnectBtn.addActionListener(new ConnectButtonListener());
        joinGameBtn.addActionListener(new JoinGameButtonListener());
        joinGameBtn.setEnabled(false);
        sendTextBtn.addActionListener(new SendButtonListener());
        sendTextBtn.setEnabled(false);
        title = BorderFactory.createTitledBorder("Write your guess here");
        guessField.setBorder(title);
        guessField.setEnabled(false);
        final String TEXT_SUBMIT = "text-submit";
        final String INSERT_BREAK = "insert-break";
        KeyStroke enter = KeyStroke.getKeyStroke("ENTER");
        InputMap input = guessField.getInputMap();
        input.put(enter, TEXT_SUBMIT);
        ActionMap actions = guessField.getActionMap();
        actions.put(TEXT_SUBMIT, new AbstractAction()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                guess(guessField.getText().trim());
                guessField.setText("");
            }
        });

        input = inputArea.getInputMap();
        enter = KeyStroke.getKeyStroke("ENTER");
        input.put(enter, TEXT_SUBMIT);

        title = BorderFactory.createTitledBorder("Draw here");
        paper.setBorder(title);
        this.add(paper, BorderLayout.CENTER);
        paper.setEnabled(false);

        actions = inputArea.getActionMap();
        actions.put(TEXT_SUBMIT, new AbstractAction()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                broadCastText();
            }
        });
        KeyStroke shiftEnter = KeyStroke.getKeyStroke("shift ENTER");
        input.put(shiftEnter, INSERT_BREAK);

        inputArea.setText("Not connected to server...");
        inputArea.setEnabled(false);

        inputArea.requestFocusInWindow();
    }
    
    /**
     * A custom class used for overriding the default behaviour when the window is closed.
     * @author Kristoffer Nordkvist
     */
    private class CloseWindowListener extends WindowAdapter
    {
        public void windowClosing(WindowEvent e)
        {
            client.disconnect();
        }
    }

    /**
     * Custom listener class for handling the event that the user uses {@link PictionaryClientGUI#disconnectOrConnectBtn}
     * @author Kristoffer Nordkvist
     */
    private class ConnectButtonListener implements ActionListener
    {
        public void actionPerformed(ActionEvent ae)
        {
            if (!connected)
            {
                try
                {
                    client.connect(PictionaryClientGUI.this);
                    sendTextBtn.setEnabled(true);
                    joinGameBtn.setEnabled(true);
                    PictionaryClientGUI.this.setTitle(client.getServerInfo());
                    inputArea.setText("");
                    inputArea.setEnabled(true);
                    inputArea.requestFocusInWindow();
                    disconnectOrConnectBtn.setText("Disconnect");
                    connected = true;
                }
                catch (IOException e)
                {
                    JOptionPane
                            .showMessageDialog(PictionaryClientGUI.this,
                                    "Could not establish connection to server, the program will exit");
                    PictionaryClientGUI.this
                            .processWindowEvent(new WindowEvent(
                                    PictionaryClientGUI.this,
                                    WindowEvent.WINDOW_CLOSING));
                }
            }
            else
            {
                client.disconnect();
                PictionaryClientGUI.this.disconnect();
            }
        }
    }

    /**
     * Called when the connection to the server is lost.
     */
    private void onServerDisconnect()
    {
        JOptionPane.showMessageDialog(PictionaryClientGUI.this,
                "Connection to the server was lost, the program will exit");
        PictionaryClientGUI.this.processWindowEvent(new WindowEvent(
                PictionaryClientGUI.this, WindowEvent.WINDOW_CLOSING));
    }
    
    /**
     * Disconnects from the server.
     */
    private void disconnect()
    {
        connected = false;
        sendTextBtn.setEnabled(false);
        joinGameBtn.setEnabled(false);
        paper.setEnabled(false);
        disconnectOrConnectBtn.setText("Connect");
        PictionaryClientGUI.this.setTitle("Not connected");
    }

    /**
     * Sends the text in {@link #inputArea} to the server
     * using {@link PictionaryClient#broadcastChatMessage(String)}.
     */
    private void broadCastText()
    {
        if (inputArea.getText().length() == 0 || !connected)
        {
            return;
        }
        client.broadcastChatMessage(inputArea.getText());
        inputArea.setText("");
        inputArea.requestFocusInWindow();
    }

    /**
     * Used when the client wants to guess on a word while in a game of Pictionary.
     * @param guess The word that is the client's guess.
     */
    private void guess(String guess)
    {
        client.guess(guess);
    }
    
    /**
     * Handles the event that is generated when the user clicks {@link PictionaryClientGUI#sendTextBtn}
     * @author Kristoffer Nordkvist
     */
    private class SendButtonListener implements ActionListener
    {

        public void actionPerformed(ActionEvent e)
        {
            broadCastText();
            inputArea.requestFocusInWindow();
        }
    }

    /**
     * Handles the event that is generated when {@link PictionaryClientGUI#joinGameBtn} is clicked.
     * @author Kristoffer Nordkvist
     */
    private class JoinGameButtonListener implements ActionListener
    {

        public void actionPerformed(ActionEvent e)
        {
            receivedText.append("Joining game...\n");
            client.joinGame();
            joinGameBtn.setEnabled(false);
        }
    }

    // --------------------------- Implementation of the IPictionaryClientCallback interface
    // --------------------------------------------------------------------------------------
    @Override
    public void correctGuessBroadcastReceived(String player, String guess)
    {
        receivedText.append(player + " knew the word was \"" + guess + "\", omg.\n");
        guessField.setEnabled(false);
    }

    @Override
    public void correctGuess()
    {
        receivedText.append("Your guess was correct. You're the man now, dog.\n");
        guessField.setEnabled(false);
    }

    @Override
    public void wrongGuessBroadcastReceived(String player, String guess)
    {
        receivedText.append(player + " guessed \"" + guess
                + "\", totally wrong.\n");
    }

    @Override
    public void aClientDisconnected(String client)
    {
        receivedText.append(client + " disconnected from the server.\n");
    }

    @Override
    public void aClientConnected(String client)
    {
        receivedText.append(client + " connected to the server.\n");
    }

    @Override
    public void serverMessageReceived(String message)
    {
        receivedText.append(message + "\n");
    }

    @Override
    public void clientBroadcastMessageReceived(String client, String message)
    {
        receivedText.append(client + " says: " + message + "\n");
    }

    @Override
    public void connectedToServer()
    {
        StringBuilder message = new StringBuilder();
        message.append("Congratulations, you successfully connected to the server.\n" +
                "Connected clients are:");
        String delim = "\n";
        for(String theClient : client.connectedClients)
        {
            message.append(delim);
            message.append(theClient);
            delim = ",\n";
        }
        message.append("\n");
        receivedText.append(message.toString());
    }

    @Override
    public void joinedNewGame(List<String> players, int gameStatus)
    {
        joinGameBtn.setEnabled(false);
        StringBuilder message = new StringBuilder();
        message.append("You joined a game of Pictionary!\n");
        if(players.size() > 0)
        {
            message.append("Connected players are:");
            String delim = "\n";
            for(String player : players)
            {
                message.append(delim);
                message.append(player);
                delim = ",\n";
            }
        }
        else
        {
            message.append("There are no other connected players.");
        }
        message.append("\n");
        if(gameStatus == PictionaryGame.NOT_ENOUGH_PLAYERS_TO_START || gameStatus == PictionaryGame.NOT_ENOUGH_PLAYERS_TO_CONTINUE)
        {
            message.append("There are not enough players to start the game.\n");
        }
        else
        {
            message.append("The game has started, wait for your turn.\n");
        }
        receivedText.append(message.toString());
    }

    @Override
    public void startDrawing(String wordToDraw, int drawTime)
    {
        guessField.setEnabled(false);
        paper.clear();
        paper.setEnabled(true);
        receivedText.append("It's your time to draw!\n");
        receivedText.append("The word is '" + wordToDraw + "'.\n");
        receivedText.append("You have " + drawTime
                + " seconds before the time is up.\n");
    }

    @Override
    public void drawReceived(Point point)
    {
        // We don't want to add a point when we're the one doing the drawing
        if (!paper.enabled)
        {
            paper.addPoint(point);
        }
    }

    @Override
    public void serverDisconnected()
    {
        onServerDisconnect();
    }

    @Override
    public void stopDrawing()
    {
        paper.setEnabled(false);
        receivedText.append("Drawing ended!\n");
    }

    @Override
    public void startGuessing()
    {
        receivedText.append("You can start guessing now!\n");
        guessField.setEnabled(true);
        guessField.requestFocusInWindow();
    }

    @Override
    public void stopGuessing()
    {
        receivedText.append("The guessing turn has ended.\n");
        guessField.setEnabled(false);
    }
    
    @Override
    public String userNameRequest()
    {
        return JOptionPane.showInputDialog(this, null, "Please enter a username", JOptionPane.INFORMATION_MESSAGE);
    }
    
    @Override
    public void clientStartedDrawing(String client)
    {
        paper.clear();
        receivedText.append(client + " is now drawing.\n");
    }
    
    @Override
    public void closedByError(String error)
    {
        receivedText.append("Connection closed by error:\n" + error + "\n");
        disconnect();
    }

    // ---------------------------------------------- end of IPictionaryClientCallback implementation
    // ------------------------------------------------------------------------------------------------
    
    /**
     * A simple class for rendering a set of points. <br />
     * Used for both receiving and displaying other players' draw points
     * as well as displaying and sending this client's points.
     */
    private class Paper extends JPanel
    {
        /**
         * The set of points that make up the drawings.
         */
        private HashSet<Point> hs = new HashSet<Point>();
        
        /**
         * True if the player can draw, else false.
         */
        private boolean enabled;
        
        /**
         * The constructor. Sets the background and sets up mouse listeners.
         */
        public Paper()
        {
            setBackground(Color.white);
            addMouseListener(new L1());
            addMouseMotionListener(new L2());
        }
        
        /**
         * Draws the points contained in {@link #hs} on the canvas.
         */
        @Override
        public void paintComponent(Graphics g)
        {
            g.setColor(Color.white);
            super.paintComponent(g);
            g.fillRect(0, 0, this.getWidth(), this.getHeight());
            g.setColor(Color.black);
            synchronized (hs)
            {
                Iterator<Point> i = hs.iterator();
                while (i.hasNext())
                {
                    Point p = (Point) i.next();
                    g.fillOval(p.x, p.y, 4, 4);
                }
            }
        }
        
        /**
         * Clears the canvas.
         */
        public void clear()
        {
            synchronized (hs)
            {
                hs = new HashSet<Point>();
                this.repaint();
            }
        }
        
        /**
         * Enables or disables
         */
        public void setEnabled(boolean enabled)
        {
            if (enabled)
            {
                this.enabled = true;
            }
            else
            {
                this.enabled = false;
            }
        }
        
        /**
         * Adds a point to {@link #hs}, thus displaying it to the user.
         * @param point {@link java.awt.Point} to be added.
         */
        public void addPoint(Point point)
        {
            synchronized (hs)
            {
                hs.add(point);
            }
            repaint();
        }
        
        /**
         * Similar to {@link #addPoint(Point)},
         * but also sends the point using {@link PictionaryClient#broadcastDraw(Point)}.
         * @param point The {@link java.awt.Point} to be added and sent.
         */
        public void addPointAndSend(Point point)
        {
            synchronized (hs)
            {
                hs.add(point);
            }
            client.broadcastDraw(point);
            repaint();
        }
        
        /**
         * Handles the {@link #mousePressed(MouseEvent)} event.
         * @author Kristoffer Nordkvist
         */
        class L1 extends MouseAdapter
        {
            /**
             * If the paper is enabled, sends the clicked point to {@link Paper#addPointAndSend(Point)}.
             */
            @Override
            public void mousePressed(MouseEvent me)
            {
                if (enabled)
                {
                    addPointAndSend(me.getPoint());
                }
            }
        }
        
        /**
         * Handles the {@link #mouseDragged(MouseEvent)} event.
         * @author Kristoffer Nordkvist
         */
        class L2 extends MouseMotionAdapter
        {
            /**
             * If the paper is enabled, sends the dragged point to {@link Paper#addPointAndSend(Point)}.
             */
            public void mouseDragged(MouseEvent me)
            {
                if (enabled)
                {
                    addPointAndSend(me.getPoint());
                }
            }
        }
    }
    
    /**
     * Asks the user for hostname and port, and then creates an instance of {@link #PictionaryClient}
     * using these values. Creates an instance of {@link PictionaryClientGUI},
     * passing the aforementioned client to its constructor.
     * @param args Not used, the values are entered in an GUI.
     */
    public static void main(String[] args)
    {
        PictionaryClient client = null;
        String hostname = "localhost";
        int port = 2012;

        /*if (args.length > 0)
        {
            hostname = args[0];
        }
        if (args.length > 1)
        {
            try
            {
                port = Integer.parseInt(args[1]);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }*/
        hostname = JOptionPane.showInputDialog(null, "Enter the hostname.\nFor example 127.0.0.1.", "A hostname is required", JOptionPane.QUESTION_MESSAGE);
        try
        {
            port =
                Integer.parseInt(
                        JOptionPane.showInputDialog(
                                null,
                                "Enter the port.\nFor example 2012.", "You need to specify the port.",
                                JOptionPane.QUESTION_MESSAGE));
        }
        catch (Exception e)
        {
            // Anv�nd default
        }
        try
        {
            client = new PictionaryClient(hostname, port);
        }
        catch (Exception e)
        {
            System.err.println("Could not start client:");
            e.printStackTrace();
            return;
        }
        final PictionaryClientGUI gui = new PictionaryClientGUI(client);

        EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                gui.setVisible(true);
            }
        });
    }
}
